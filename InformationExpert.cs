﻿using PlayServer;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindPlay2._0
{
    class InformationExpert
    {
        TeamList teamList;
        PlayerList playerList;
        AuthDataList authDataList;
        NotEndedMatchList notEndedMatchList;
        QueueTeams queue;

        public Tournament tour = null;

        public void CreateTeam(Team team)
        {
            teamList.AddTeam(team);
        }

        public bool IsPlayerInList(Player player)
        {
            return playerList.IsPlayerInList(player);
        }
        public void AddPlayer(Player player)
        {
            playerList.AddPlayer(player);
        }


        public void AddInQueue(Player player)
        {
            queue.AddInQueueTeams(teamList.GetTeam(player));
        }
        public void RemoveFromQueue(Player player)
        {
            queue.RemoveFromQueueTeams(player);
        }

        public bool IsInQueue(Player player)
        {
            return queue.IsInQueue(player);
        }

        public void CreateMatchOfEquelsTeams()
        {
            List<Team> teams = queue.GetEquelsTeams();
            if(teams!=null) notEndedMatchList.AddMatch(new Match(teams));
        }

        public void AddNotEndedMatch(Match match)
        {
            notEndedMatchList.AddMatch(match);
        }
        public Match GetNotEndedMatch(Player player)
        {
            return notEndedMatchList.GetNotEndedMatch(player);
        }
        public void EndMatch(Player player)
        {
            //Начисление рейтинга всем игрокам через функцию getTeamsFromNotEndedMatch
            notEndedMatchList.EndMatch(player);
        }

        

        public bool Auth(string name, string pass)
        {
            return authDataList.Auth(name, pass);
        }
        public List<Player> GetPlayers(List<string> names)
        {
            try
            {
                return playerList.GetPlayers(names);
            }
            catch
            {
                return null;
            }
        }
        public bool TeamNameIsBusy(string name)
        {
            return teamList.NameIsBusy(name);
        }
        public bool IsPlayersInTeam(List<Player> players)
        {
            foreach(Player player in players)
            {
                if (teamList.AlreadyInTeam(player)) return false;
            }
            return true;
        }
        public Team GetTeam(Player player)
        {
            return teamList.GetTeam(player);
        }
        public Player GetPLayerFromName(string name)
        {
            return playerList.GetPlayerFromName(name);
        }

        public void Init()
        {
            queue = new QueueTeams();
            teamList = new TeamList();
            playerList = new PlayerList();
           // authDataList = new AuthDataList();
            notEndedMatchList = new NotEndedMatchList();
            playerList.AddPlayers(new List<Player> {new Player("player1", 100, "contacts"), new Player("player2", 125, "contacts"),
                new Player("player3", 350, "contacts"), new Player("player4", 50, "contacts"), new Player("player5", 125, "contacts")});
            playerList.AddPlayers(new List<Player> {new Player("player6", 25, "contacts"), new Player("player7", 50, "contacts"),
                new Player("player8", 175, "contacts"), new Player("player9", 325, "contacts"), new Player("player10", 400, "contacts")});
            playerList.AddPlayers(new List<Player> {new Player("player11", 75, "contacts"), new Player("player12", 0, "contacts"),
                new Player("player13", 25, "contacts"), new Player("player14", 100, "contacts"), new Player("player15", 125, "contacts")});
            playerList.AddPlayers(new List<Player> {new Player("player16", 500, "contacts"), new Player("player17", 525, "contacts"),
                new Player("player18", 350, "contacts"), new Player("player19", 50, "contacts"), new Player("player20", 200, "contacts")});
            playerList.AddPlayers(new List<Player> {new Player("player21", 150, "contacts"), new Player("player22", 125, "contacts"),
                new Player("player23", 450, "contacts"), new Player("player24", 50, "contacts"), new Player("player25", 125, "contacts")});
            playerList.AddPlayers(new List<Player> {new Player("player26", 300, "contacts"), new Player("player27", 325, "contacts"),
                new Player("player28", 150, "contacts"), new Player("player29", 50, "contacts"), new Player("player30", 200, "contacts")});
            playerList.AddPlayers(new List<Player> {new Player("player31", 150, "contacts"), new Player("player32", 125, "contacts"),
                new Player("player33", 350, "contacts"), new Player("player34", 150, "contacts"), new Player("player35", 100, "contacts")});
            playerList.AddPlayers(new List<Player> {new Player("player36", 25, "contacts"), new Player("player37", 125, "contacts"),
                new Player("player38", 125, "contacts"), new Player("player39", 150, "contacts"), new Player("player40", 25, "contacts")});





            teamList.AddTeam(new Team(new List<Player> {new Player("player1", 100, "contacts"), new Player("player2", 125, "contacts"),
                new Player("player3", 350, "contacts"), new Player("player4", 50, "contacts"), new Player("player5", 125, "contacts")}, "team1"));
            teamList.AddTeam(new Team(new List<Player> {new Player("player6", 25, "contacts"), new Player("player7", 50, "contacts"),
                new Player("player8", 175, "contacts"), new Player("player9", 325, "contacts"), new Player("player10", 400, "contacts")}, "team2"));
            teamList.AddTeam(new Team(new List<Player> {new Player("player11", 75, "contacts"), new Player("player12", 0, "contacts"),
                new Player("player13", 25, "contacts"), new Player("player14", 100, "contacts"), new Player("player15", 125, "contacts")}, "team3"));
            teamList.AddTeam(new Team(new List<Player> {new Player("player16", 500, "contacts"), new Player("player17", 525, "contacts"),
                new Player("player18", 350, "contacts"), new Player("player19", 50, "contacts"), new Player("player20", 200, "contacts")}, "team4"));
            teamList.AddTeam(new Team(new List<Player> {new Player("player21", 150, "contacts"), new Player("player22", 125, "contacts"),
                new Player("player23", 450, "contacts"), new Player("player24", 50, "contacts"), new Player("player25", 125, "contacts")}, "team5"));
            teamList.AddTeam(new Team(new List<Player> {new Player("player26", 300, "contacts"), new Player("player27", 325, "contacts"),
                new Player("player28", 150, "contacts"), new Player("player29", 50, "contacts"), new Player("player30", 200, "contacts")}, "team6"));
            teamList.AddTeam(new Team(new List<Player> {new Player("player31", 150, "contacts"), new Player("player32", 125, "contacts"),
                new Player("player33", 350, "contacts"), new Player("player34", 150, "contacts"), new Player("player35", 100, "contacts")}, "team7"));
            teamList.AddTeam(new Team(new List<Player> {new Player("player36", 25, "contacts"), new Player("player37", 125, "contacts"),
                new Player("player38", 125, "contacts"), new Player("player39", 150, "contacts"), new Player("player40", 25, "contacts")}, "team8"));
        }
    }
}
