﻿using FindPlay2._0;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayServer
{
    class NotEndedMatchList
    {
        List<Match> matches;

        public NotEndedMatchList()
        {
            matches = new List<Match>();
        }

        public void AddMatch(Match match)
        {
            matches.Add(match);
        }

        public Match GetNotEndedMatch(Player player) 
        {
            return matches.Find(m => m.teams.Contains(m.teams.Find(t => t.players.Contains(t.players.Find(p=>p.name==player.name)))));
        }
        public void EndMatch(Player player)
        {
            
            matches.Remove(matches.Find(m => m.teams.Contains(m.teams.Find(t => t.players.Contains(t.players.Find(p => p.name == player.name))))));
        }

        public List<Team> GetTeamsFromNotEndedMatch(Player player)
        {
            return matches.Find(m => m.teams.Contains(m.teams.Find(t => t.players.Contains(t.players.Find(p => p.name == player.name))))).teams;
        }

     
    }
}
