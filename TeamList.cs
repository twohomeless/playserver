﻿using FindPlay2._0;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayServer
{
    class TeamList
    {
        List<Team> teamList;

        public TeamList()
        {
            teamList = new List<Team>();
        }

        public void AddTeam(Team team)
        {
            teamList.Add(team);
        }
        public bool NameIsBusy(string name)
        {
            if (teamList.Find(t => t.name == name) != null) return true;
            return false;
        }
        public bool AlreadyInTeam(Player player)
        {
            var temp = teamList.Find(t => t.players.Contains(t.players.Find(p => p.name == player.name)));
            if (temp!=null) return true;
            return false;
        }

        public Team GetTeam(Player player)
        {
            return teamList.Find(t => t.players.Contains(t.players.Find(p=>p.name==player.name)));
        }
    }
}
