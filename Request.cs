﻿using FindPlay2._0;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace PlayServer
{
    class Request
    {

        public Player player;
        public string password;
        public string action;
        public bool result;
        public Team myTeam;
        public Match currentMatch;
        public Tournament tour;

        

        public Request(byte[] jsonInByte)
        {
            string json = Encoding.UTF8.GetString(jsonInByte, 0, jsonInByte.Length);
            var qwe = JsonConvert.DeserializeObject<Request>(json);
            player = qwe.player;
            password = qwe.password;
            action = qwe.action;
            result = qwe.result;
            myTeam = qwe.myTeam;
            currentMatch = qwe.currentMatch;
            tour = qwe.tour;
        }

        public Request()
        {

        }
        [JsonConstructor]
        public Request(Player player, string password, string action, bool result, Team myTeam, Match currentMatch, Tournament tour)
        {
            this.player = player;
            this.password = password;
            this.action = action;
            this.result = result;
            this.myTeam = myTeam;
            this.currentMatch = currentMatch;
            this.tour = tour;
        }

        public byte[] RequestSerialize()
        {
            string json = JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
            return Encoding.UTF8.GetBytes(json);
        }
    }

}

