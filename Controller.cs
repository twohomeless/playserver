﻿using FindPlay2._0;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PlayServer
{
    class Controller
    {
        //!
        public InformationExpert infoExp;
        public TcpClient client;
        const int port = 8888;
        static TcpListener listener;

        public Controller()
        {
            infoExp = new InformationExpert();
            infoExp.Init();
        }

        public void GetRequest()
        {
            //try
            //{
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), port);
                listener.Start();
                Console.WriteLine("Ожидание подключений...");

                while (true)
                {
                    client = listener.AcceptTcpClient();
                    NetworkStream stream = null;
                    stream = client.GetStream();
                    byte[] data = new byte[2000];
                    do
                    {
                        stream.Read(data, 0, data.Length);
                    }
                    while (stream.DataAvailable);
                    Request request = new Request(data);
                    switch (request.action)
                    {
                        case "auth":
                            {
                                Request answer = Auth(request);
                                stream.Write(answer.RequestSerialize(), 0, answer.RequestSerialize().Length); break;
                            }
                        case "addPlayer":
                            {
                                Request answer = AddPlayer(request);
                                stream.Write(answer.RequestSerialize(), 0, answer.RequestSerialize().Length); break;
                            }
                        case "getTeamInfo":
                            {
                                Request answer = GetTeamInfo(request);
                                stream.Write(answer.RequestSerialize(), 0, answer.RequestSerialize().Length); break;
                            }
                        case "createTeam":
                            {
                                Request answer = CreateTeam(request);
                                stream.Write(answer.RequestSerialize(), 0, answer.RequestSerialize().Length); break;
                            }
                        case "getCurrentMatchInfo":
                            {
                                Request answer = GetCurrentMatchInfo(request);
                                stream.Write(answer.RequestSerialize(), 0, answer.RequestSerialize().Length); break;
                            }
                        case "addInQueue":
                            {
                                Request answer = AddInQueue(request);
                                stream.Write(answer.RequestSerialize(), 0, answer.RequestSerialize().Length); break;
                            }
                        case "leaveFromQueue":
                            {
                                Request answer = LeaveFromQueue(request);
                                stream.Write(answer.RequestSerialize(), 0, answer.RequestSerialize().Length); break;
                            }
                        case "endGame":
                            {
                                Request answer = EndGame(request);
                                stream.Write(answer.RequestSerialize(), 0, answer.RequestSerialize().Length); break;
                            }
                    }
                }
            //}
            ///catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            //finally
           // {
            //    if (listener != null)
            //        listener.Stop();
            //}
        }

        public Request Auth(Request request)
        {
            Request answer = new Request();
            if (infoExp.Auth(request.player.name, request.password))
            { 
                answer.result = true;
                answer.player = GetPlayerFromName(request.player.name);

            }
            else
            {
                answer.result = false;
            }
            return answer;
        }

        public Request AddPlayer(Request request)
        {
            Player player = GetPlayerFromName(request.player.name);
            Request answer = new Request();
            answer.player = player;
            if (infoExp.IsPlayerInList(player)) answer.result = false;
            else
            {
                answer.result = true;
                infoExp.AddPlayer(request.player);
            }
            return answer;
        }

        public Request GetTeamInfo(Request request)
        {
            Player player = GetPlayerFromName(request.player.name);
            Request answer = new Request();
            answer.player = player;
            answer.result = true;
            answer.myTeam = infoExp.GetTeam(player);
            return answer;
        }
        public Player GetPlayerFromName(string name)
        {
            return infoExp.GetPLayerFromName(name);
        }

        public Request CreateTeam(Request request)
        {
            //не по прецеденту
            Request answer = new Request();
            answer.player = request.player;
            if (infoExp.TeamNameIsBusy(request.myTeam.name))
            {
                answer.result = false;
            }
            else if(infoExp.IsPlayersInTeam(request.myTeam.players))
            {
                answer.result = false;
            }
            else
            {
                infoExp.CreateTeam(request.myTeam);
                answer.result = true;
                answer.myTeam = request.myTeam;
            }
            return answer;
        }

        public Request GetCurrentMatchInfo(Request request)
        {
            Request answer = new Request();
            Player player = GetPlayerFromName(request.player.name);
            answer.player = player;
            answer.result = true;
            answer.currentMatch = infoExp.GetNotEndedMatch(player);
            return answer;
        }

        public Request AddInQueue(Request request)
        {
            Request answer = new Request();
            answer.player = request.player;
            infoExp.AddInQueue(request.player);
            infoExp.CreateMatchOfEquelsTeams();
            answer.result = true;
            return answer;
        }
        
        public Request LeaveFromQueue(Request request)
        {
            Request answer = new Request();
            answer.player = request.player;
            if (infoExp.IsInQueue(request.player))
            {
                infoExp.RemoveFromQueue(request.player);
                answer.result = true;
            }
            else
            {
                answer.result = false;
            }
            return answer;
        }

        public Request EndGame(Request request)
        {
            Request answer = new Request();
            answer.player = request.player;
            //Начисление рейтинга
            infoExp.EndMatch(request.player);
            answer.result = true;
            return answer;
         
        }

        public void GetResult(Player player)
        {
            if (infoExp.tour.IsInTour(player))
            {
                //получение данных парсинга и добавление команды победителя в новый матч турнира
            }
            else
            {
                //Обычное начисление рейтинга
            }
        }

        public bool CreateTeam(List<String> PlayerNames, string teamName)
        {
            if (infoExp.TeamNameIsBusy(teamName)) return false;
            List<Player> players = infoExp.GetPlayers(PlayerNames);
            if (!infoExp.IsPlayersInTeam(players)) return false;
            if (players.Count != 5) return false;
            infoExp.CreateTeam(new Team(players, teamName));
            return true;
        }



        
    }
}
