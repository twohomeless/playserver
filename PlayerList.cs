﻿using FindPlay2._0;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayServer
{
    class PlayerList
    {
        List<Player> players;

        public PlayerList()
        {
            players = new List<Player>();
        }
        public Player GetPlayerFromName(string name)
        {
            return players.Find(p => p.name == name);
        }

        public List<Player> GetPlayers(List<String> names)
        {
            List<Player> players = new List<Player>();
            foreach(string name in names)
            {
                try
                {
                    players.Add(players.Find(p => p.name == name));
                }catch(Exception e)
                {
                    throw(new Exception("Player not found"));
                }
            }
            return players;
        }

        public bool IsPlayerInList(Player player)
        {
            if (players.Contains(player)) return true;
            else return false;
        }

        public void AddPlayer(Player player)
        {
            players.Add(player);
        }
        public void AddPlayers(List<Player> players)
        {
            this.players.AddRange(players);
        }
    }
}
