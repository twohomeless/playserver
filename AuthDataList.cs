﻿using FindPlay2._0;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayServer
{
    class AuthDataList
    {
        Dictionary<string, string> authDataList;

        public AuthDataList(Dictionary<string, string> authDataList)
        {
            this.authDataList = authDataList;
        }

        public bool Auth(string name, string pass)
        {
            if (authDataList[name] == pass)
            {
                return true;
            }
            return false;
        }
    }
}
