﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindPlay2._0
{
    class Match
    {
        public List<Team> teams;
        public Team winner;
        public Match(List<Team> teams)
        {
            this.teams = teams;
        }
        public void SetWinner(Team team)
        {
            winner = team;
        }
    }
}
