﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindPlay2._0
{
    class Team
    {
        public string name;
        public List<Player> players;

        public Team(List<Player> players, string name)
        {
            this.name = name;
            this.players = players;
        }


        public int GetRating()
        {
            int rating;
            rating = players.Sum(p => p.rating);
            return rating;
        }
    }
}
