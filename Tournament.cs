﻿using FindPlay2._0;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayServer
{
    class Tournament
    {
        List<Match> matches;

        public Tournament()
        {
            matches = new List<Match>();
        }

        public void AddTeamToTour(Team team)
        {
            matches.Find(m => m.teams.Count < 2).teams.Add(team);
        }
        public bool IsEnought()
        {
            if (matches.Count == 4)
            {
                return true;
            }
            return false;
        }

        public bool IsInTour(Player player)
        {
            if (matches.Where(m => m.teams.Where(t => t.players.Contains(player)) == null).Count() != 0) return true;
            return false;
        }
    }
}
