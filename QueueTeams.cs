﻿using FindPlay2._0;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayServer
{
    class QueueTeams
    {
        List<Team> teams;

        public QueueTeams()
        {
            teams = new List<Team>();
        }

        public void AddInQueueTeams(Team team)
        {
            Console.WriteLine("Команда встала в очередь: "+team.name+" рейтинг: "+team.players.Sum(p=>p.rating));
            teams.Add(team);
            teams.OrderBy(t => t.GetRating()); 
        }

        public void RemoveFromQueueTeams(Player player)
        {
            teams.Remove(teams.Find(t => t.players.Contains(t.players.Find(p=>p.name==player.name))));
        }

        public List<Team> GetEquelsTeams()
        {
            List<Team> teamsToMatch = new List<Team>();
            for (int i = 0; i < teams.Count-1; i++)
            {
                if (Math.Abs(teams[i].GetRating() - teams[i + 1].GetRating()) <= 300)
                {
                    teamsToMatch.Add(teams[i]);
                    teamsToMatch.Add(teams[i + 1]);
                    teams.Remove(teams[i]);
                    teams.Remove(teams[i]);
                    return teamsToMatch;
                }
            }
            return null;
        }

        public bool IsInQueue(Player player)
        {
            if (teams.Contains(teams.Find(t => t.players.Contains(t.players.Find(p=>p.name==player.name))))) return true;
            else return false;
        }


    }
}
